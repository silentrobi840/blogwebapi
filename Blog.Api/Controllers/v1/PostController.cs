using System;
using Blog.Api.ErrorModels;
using Blog.Application.Interfaces;
using Blog.Domain.Dtos;
using Blog.Domain.Entities;
using Blog.Domain.Interfaces;
using Blog.Domain.QueryMapper;
using Microsoft.AspNetCore.Mvc;
namespace Blog.Api.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/blog/posts")]
    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        [HttpGet]
        public ActionResult GetPosts([FromQuery] QueryOptions options)
        {
            return Ok(_postService.GetPosts(options));
        }

        [HttpGet("{id}")]
        public ActionResult GetPost(string id)
        {
            return Ok(_postService.GetPostById(id));
        }

        [HttpPost]
        public ActionResult CreatePost([FromBody] PostDto postDto)
        {
            var (message, isSucceed) = _postService.CreatePost(postDto);

            if (!isSucceed)
            {
                return BadRequest(new CreateError
                {   
                    Title = "Create Error",
                    Description = message
                });
            }

            return Ok(message);
        }

        [HttpPut("{id}")]
        public ActionResult UpdatePost(string id, [FromBody] PostDto postDto)
        {
            var (message, isSucceed) = _postService.UpdatePostById(id, postDto);

            if (!isSucceed) return NotFound(message);

            return Ok(message);
        }

        [HttpDelete("{id}")]
        public ActionResult DeletePost(string id)
        {
            var (message, isSucceed) = _postService.DeletePostById(id);

            if (!isSucceed) return NotFound(message);

            return Ok(message);
        }


        [HttpPost("{id}/like")]
        public ActionResult LikePost(string id, [FromQuery] string userId)
        {
            var (message, isSucceed) = _postService.LikePost(id, userId);

            if (!isSucceed) return BadRequest(message);

            return Ok(message);
        }

        [HttpPost("{id}/unlike")]
        public ActionResult UnlikePost(string id, [FromQuery] string userId)
        {
            var (message, isSucceed) = _postService.UnlikePost(id, userId);

            if (!isSucceed) return BadRequest(message);

            return Ok(message);
        }
    }
}