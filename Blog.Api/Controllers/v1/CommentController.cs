﻿using Blog.Application.Interfaces;
using Blog.Domain.Dtos;
using Blog.Domain.Entities;
using Blog.Domain.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Blog.Api.Controllers.v1
{
    [ApiController]
    [Route("api/v{version:apiVersion}/blog/posts/{postId}/comments")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpPost]
        public ActionResult CreateComment(string postId, [FromBody] CommentDto commentDto)
        {
            _commentService.CreateComment(postId, commentDto);
            return Ok();
        }

        [HttpGet]
        public ActionResult GetComments(string postId)
        {
            return Ok(_commentService.GetComments(postId));
        }

        [HttpGet("{id}")]
        public ActionResult GetComment(string postId, string id)
        {
            return Ok(_commentService.GetCommentById(postId, id));
        }

        [HttpPut("{id}")]
        public ActionResult UpdateComment(string postId, string id, [FromBody] CommentDto commentDto)
        {
            var (message, isSucceed) = _commentService.UpdateCommentById(postId, id, commentDto);

            if (!isSucceed) return NotFound(message);

            return Ok(message);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteComment(string postId, string id)
        {
            var (message, isSucceed) = _commentService.DeleteCommentById(postId, id);

            if (!isSucceed) return NotFound(message);

            return Ok(message);
        }

        [HttpPost("{id}/like")]
        public ActionResult LikeComment(string postId, string id, [FromQuery] string userId)
        {
            var (message, isSucceed) = _commentService.LikeComment(postId, id, userId);

            if (!isSucceed) return BadRequest(message);

            return Ok(message);
        }

        [HttpPost("{id}/unlike")]
        public ActionResult UnlikeComment(string postId, string id, [FromQuery] string userId)
        {
            var (message, isSucceed) = _commentService.UnlikeComment(postId, id, userId);

            if (!isSucceed) return BadRequest(message);

            return Ok(message);
        }
    }
}
