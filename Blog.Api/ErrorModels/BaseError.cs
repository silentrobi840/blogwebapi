namespace Blog.Api.ErrorModels
{
    public class BaseError {
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
    }
}