namespace Blog.Domain.Interfaces
{
    public interface IBlogUnitOfWork
    {
        IPostRepository PostRepository { get; }
        ICommentRepository CommentRepository { get; }
        IUserRepository UserRepository { get; }
    }
}