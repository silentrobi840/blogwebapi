using Blog.Domain.Entities;
using System.Collections.Generic;

namespace Blog.Domain.Interfaces
{
    public interface ICommentRepository
    {
        void Create(string postId, Comment entity);

        IEnumerable<Comment> GetMultiple(string postId);

        Comment Get(string postId, string id);

        bool Update(string postId, string id, Comment entity);

        bool Delete(string postId, string id);

    }
}