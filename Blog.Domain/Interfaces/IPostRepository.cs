using Blog.Domain.Entities;

namespace Blog.Domain.Interfaces
{
    public interface IPostRepository: ICrudRepository<string, Post>
    {
  
    }
}
