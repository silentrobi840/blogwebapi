using Blog.Domain.Entities;

namespace Blog.Domain.Interfaces
{
    public interface IUserRepository : ICrudRepository<string, User>
    {
        
    }
}