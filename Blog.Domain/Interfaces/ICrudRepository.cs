﻿using System.Collections.Generic;
using Blog.Domain.QueryMapper;
namespace Blog.Domain.Interfaces
{
    public interface ICrudRepository<T, TEntity>
    {
        void Create(TEntity entity);

        IEnumerable<TEntity> GetMultiple(QueryOptions options);
        
        TEntity Get(T id);
        
        bool Update(T id, TEntity entity);
        
        bool Delete(T id);
    }
}
