﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Blog.Domain.Entities
{
    public class User: BaseEntity
    {
        public string Email { get; set; }

        public string SecurePassword { get; set; }

        public string PhoneNo { get; set; }
        
        public string Name { get; set; }

        public string BirthDate { get; set; }

        public string Profession { get; set; } 
    }
}
