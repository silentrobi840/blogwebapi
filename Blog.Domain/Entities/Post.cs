using AutoMapper;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Blog.Domain.Entities
{
    public class Post : BaseEntity
    {
        public string Content { get; set; }

        public List<string> Uploads { get; set; }

        public List<string> LikedUsers { get; set; }

        public List<Comment> Comments { get; set; }

        public string UserId { get; set; }
    }
}