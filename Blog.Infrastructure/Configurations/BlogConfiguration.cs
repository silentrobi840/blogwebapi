namespace Blog.Infrastructure.Configurations {
    public class BlogConfiguration {
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }
}