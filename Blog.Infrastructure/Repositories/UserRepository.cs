using System.Collections.Generic;
using Blog.Domain.Interfaces;
using Blog.Domain.Entities;
using Blog.Infrastructure.Contexts;
using MongoDB.Driver;
using System;
using Blog.Domain.QueryMapper;

namespace Blog.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IBlogContext _context;

        public UserRepository(IBlogContext context)
        {
            _context = context;
        }

        public void Create(User entity)
        {
            try
            {
                _context.Users.InsertOne(entity);
            }
            catch (MongoWriteConcernException ex)
            {
                throw ex;
            }
        }

        public bool Delete(string id)
        {
           try
            {
                DeleteResult actionResult = _context.Users.DeleteOne(user => user.Id == id);
                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User Get(string id)
        {
            try
            {
                return _context.Users.Find(user => user.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<User> GetMultiple(QueryOptions options)
        {
            try
            {
                return _context.Users.Find(user => true).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(string id, User entity)
        {
            try
            {
                ReplaceOneResult actionResult = _context.Users.ReplaceOne(user => user.Id == id, entity);
                return actionResult.IsAcknowledged && actionResult.MatchedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}