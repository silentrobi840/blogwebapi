using Blog.Domain.Interfaces;

namespace Blog.Infrastructure.Repositories
{
    public class BlogUnitOfWork : IBlogUnitOfWork
    {
        public BlogUnitOfWork(
        IPostRepository postRepository,
        ICommentRepository commentRepository,
        IUserRepository userRepository)
        {
            PostRepository = postRepository;
            CommentRepository = commentRepository;
            UserRepository = userRepository;
        }

        public IPostRepository PostRepository { get; }

        public ICommentRepository CommentRepository { get; }

        public IUserRepository UserRepository { get; }

    }
}