
using System.Collections.Generic;
using System;
using Blog.Domain.Entities;
using Blog.Domain.Interfaces;
using Blog.Infrastructure.Contexts;
using MongoDB.Driver;
using Blog.Domain.QueryMapper;
using System.Linq.Expressions;
using MongoDB.Driver.Linq;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Reflection;

namespace Blog.Infrastructure.Repositories
{
    public class PostRepository : IPostRepository
    {
        private readonly IBlogContext _context;

        public PostRepository(IBlogContext context)
        {
            _context = context;
        }

        public void Create(Post entity)
        {
            try
            {
                _context.Posts.InsertOne(entity);
            }
            catch (MongoWriteConcernException ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Post> GetMultiple(QueryOptions options)
        {
            try
            {
                if (options.Sort?.Trim().ToLower() == "asc")
                {
                    PropertyInfo sortByProperty = typeof(Post).GetProperty(options.SortBy?.Trim() ?? "UpdatedAt") ?? typeof(Post).GetProperty("UpdatedAt");
                    if (options.UserId == null)
                    {
                        return _context.Posts.AsQueryable().OrderBy(sortByProperty.Name).Take(options.Limit).ToList();
                    }
                    else
                    {
                        return _context.Posts.AsQueryable().Where(post => post.UserId == options.UserId).OrderBy(sortByProperty.Name).Take(options.Limit).ToList();
                    }
                }
                else
                {
                    var sortByProperty = typeof(Post).GetProperty(options.SortBy?.Trim() ?? "UpdatedAt") ?? typeof(Post).GetProperty("UpdatedAt");

                    if (options.UserId == null)
                    {
                        System.Diagnostics.Debug.WriteLine("hi");
                        return _context.Posts.AsQueryable().OrderBy($"{sortByProperty.Name} DESC").Take(options.Limit).ToList();
                    }
                    else
                    {
                        return _context.Posts.AsQueryable().Where(post => post.UserId == options.UserId)
                                .OrderBy($"{sortByProperty.Name} DESC").Take(options.Limit).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Post Get(string id)
        {
            try
            {
                return _context.Posts.Find(post => post.Id == id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Update(string id, Post entity)
        {
            try
            {
                ReplaceOneResult actionResult = _context.Posts.ReplaceOne(post => post.Id == id, entity);
                return actionResult.IsAcknowledged && actionResult.MatchedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                DeleteResult actionResult = _context.Posts.DeleteOne(post => post.Id == id);
                return actionResult.IsAcknowledged && actionResult.DeletedCount > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}