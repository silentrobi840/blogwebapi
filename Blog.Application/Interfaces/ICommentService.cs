using Blog.Domain.Dtos;
using Blog.Domain.Entities;
using System.Collections.Generic;

namespace Blog.Application.Interfaces
{
    public interface ICommentService
    {
        void CreateComment(string postId, CommentDto commentDto);

        CommentDto GetCommentById(string postId, string id);

        IEnumerable<CommentDto> GetComments(string postId);

        (string, bool) UpdateCommentById(string postId, string id, CommentDto commentDto);

        (string, bool) DeleteCommentById(string postId, string id);

        (string, bool) LikeComment(string postId, string id, string userId);

        (string, bool) UnlikeComment(string postId, string id, string userId);
    }
}