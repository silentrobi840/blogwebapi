using Blog.Domain.Dtos;
using Blog.Domain.Entities;
using Blog.Domain.QueryMapper;
using System.Collections.Generic;

namespace Blog.Application.Interfaces
{
    public interface IPostService
    {
        (string, bool) CreatePost(PostDto postDto);

        PostDto GetPostById(string id);

        IEnumerable<PostDto> GetPosts(QueryOptions options);

        (string, bool) UpdatePostById(string id, PostDto postDto);

        (string, bool) DeletePostById(string id);

        (string, bool) LikePost(string id, string userId);
        (string, bool) UnlikePost(string id, string userId);
    }
}