using System.Collections.Generic;
using Blog.Domain.Dtos;
using Blog.Domain.QueryMapper;

namespace Blog.Application.Interfaces
{
    public interface IUserService
    {
         void CreateUser(UserDto userDto);

         UserDto GetUserById(string id);

         IEnumerable<UserDto> GetUsers(QueryOptions options);

         (string, bool) UpdateUserById(string id, UserDto userDto);

         (string, bool) DeleteUserById(string id);
    }
}