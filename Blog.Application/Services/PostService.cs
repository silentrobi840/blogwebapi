using AutoMapper;
using Blog.Application.Interfaces;
using Blog.Domain.Dtos;
using Blog.Domain.Entities;
using Blog.Domain.Interfaces;
using Blog.Domain.QueryMapper;
using System;
using System.Collections.Generic;

namespace Blog.Application.Services
{
    public class PostService : IPostService
    {
        private readonly IBlogUnitOfWork _blogUnitOfWork;
        private readonly IMapper _mapper;

        public PostService(IMapper mapper, IBlogUnitOfWork blogUnitOfWork)
        {
            _blogUnitOfWork = blogUnitOfWork;
            _mapper = mapper;
        }

        public (string, bool) CreatePost(PostDto postDto)
        {
            Post entity = _mapper.Map<Post>(postDto);

            if (entity.UserId == null)
            {
                return ("Failed to create post: User Id is missing", false);
            }

            User userEntity = _blogUnitOfWork.UserRepository.Get(entity.UserId);

            if (userEntity == null)
            {
                return ("Failed to create post: User Id is not valid", false);
            }

            entity.CreatedAt = DateTime.Now;
            entity.UpdatedAt = DateTime.Now;
            entity.Comments = new List<Comment>();
            entity.Uploads = postDto.Uploads == null || postDto.Uploads.Count == 0 ? new List<string>() : postDto.Uploads;
            entity.LikedUsers = new List<string>();

            _blogUnitOfWork.PostRepository.Create(entity);

            return ("New post created successfully", true);
        }

        public (string, bool) DeletePostById(string id)
        {
            var isSucceed = _blogUnitOfWork.PostRepository.Delete(id);

            if (!isSucceed) return ("Post is not deleted successfully", false);

            return ("Post is deleted successfully", false);
        }

        public PostDto GetPostById(string id)
        {
            Post entity = _blogUnitOfWork.PostRepository.Get(id);

            return _mapper.Map<PostDto>(entity);
        }

        public IEnumerable<PostDto> GetPosts(QueryOptions options)
        {
            var entities = _blogUnitOfWork.PostRepository.GetMultiple(options);

            return _mapper.Map<IEnumerable<PostDto>>(entities);
        }

        public (string, bool) UpdatePostById(string id, PostDto postDto)
        {
            Post entity = _blogUnitOfWork.PostRepository.Get(id);

            if (entity == null) return ("No Post is found to update", false);

            entity.Content = postDto.Content ?? entity.Content;
            entity.UpdatedAt = DateTime.Now;
            entity.Uploads = postDto.Uploads == null || postDto.Uploads.Count == 0 ? entity.Uploads : postDto.Uploads;

            var isSucceed = _blogUnitOfWork.PostRepository.Update(id, entity);

            if (!isSucceed) return ("Post is not updated successfully", false);

            return ("Post is updated successfully", true);
        }

        public (string, bool) LikePost(string id, string userId)
        {
            if (userId == null) return ("Failed to like: User Id is missing", false);

            User userEntity = _blogUnitOfWork.UserRepository.Get(userId);

            if (userEntity == null)
            {
                return ("Failed to like: User Id is not valid", false);
            }

            Post postEntity = _blogUnitOfWork.PostRepository.Get(id);

            if (postEntity == null) return ("No Post is found to like", false);

            if (postEntity.LikedUsers.Contains(userId)) return ("User have already liked the post", false);

            postEntity.LikedUsers.Add(userId); // adding in array

            var isSucceed = _blogUnitOfWork.PostRepository.Update(id, postEntity);

            if (!isSucceed) return ("Like is not added successfully", false);

            return ("Like is added successfully", true);
        }

        public (string, bool) UnlikePost(string id, string userId)
        {
            if (userId == null) return ("Failed to unLike: User Id is missing", false);

            User userEntity = _blogUnitOfWork.UserRepository.Get(userId);

            if (userEntity == null)
            {
                return ("Failed to unlike: User Id is not valid", false);
            }

            Post postEntity = _blogUnitOfWork.PostRepository.Get(id);

            if (postEntity == null) return ("No Post is found to unlike", false);

            if (!postEntity.LikedUsers.Contains(userId)) return ("Failed to perform operation as no like is found with given User Id", false);

            postEntity.LikedUsers.Remove(userId);

            var isSucceed = _blogUnitOfWork.PostRepository.Update(id, postEntity);

            if (!isSucceed) return ("Like is not removed successfully", false);

            return ("Like is removed successfully", true);
        }
    }
}