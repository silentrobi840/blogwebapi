using AutoMapper;
using Blog.Application.Interfaces;
using Blog.Domain.Dtos;
using Blog.Domain.Entities;
using Blog.Domain.Interfaces;
using System;
using System.Collections.Generic;

namespace Blog.Application.Services
{
    public class CommentService : ICommentService
    {
        private readonly IBlogUnitOfWork _blogUnitOfWork;
        private readonly IMapper _mapper;

        public CommentService(IMapper mapper, IBlogUnitOfWork blogUnitOfWork)
        {
            _blogUnitOfWork = blogUnitOfWork;
            _mapper = mapper;
        }

        public void CreateComment(string postId, CommentDto commentDto)
        {
            Comment entity = _mapper.Map<Comment>(commentDto);
            entity.CreatedAt = DateTime.Now;
            _blogUnitOfWork.CommentRepository.Create(postId, entity);
        }

        public (string, bool) DeleteCommentById(string postId, string id)
        {
            var isSucceed = _blogUnitOfWork.CommentRepository.Delete(postId, id);

            if (!isSucceed) return ("Post is not deleted successfully", false);

            return ("Post is deleted successfully", false);
        }

        public CommentDto GetCommentById(string postId, string id)
        {
            Comment entity = _blogUnitOfWork.CommentRepository.Get(postId, id);

            return _mapper.Map<CommentDto>(entity);
        }

        public IEnumerable<CommentDto> GetComments(string postId)
        {
            var entities = _blogUnitOfWork.CommentRepository.GetMultiple(postId);

            return _mapper.Map<IEnumerable<CommentDto>>(entities);
        }

        public (string, bool) LikeComment(string postId, string id, string userId)
        {
            if (userId == null) return ("Failed to like: User Id is missing", false);

            User userEntity = _blogUnitOfWork.UserRepository.Get(userId);

            if (userEntity == null)
            {
                return ("Failed to like: User Id is not valid", false);
            }

            Comment commentEntity = _blogUnitOfWork.CommentRepository.Get(postId, id);

            if (commentEntity == null) return ("No Comment is found to like", false);

            if (commentEntity.LikedUsers.Contains(userId)) return ("User have already liked the comment", false);

            commentEntity.LikedUsers.Add(userId); // adding in array

            var isSucceed = _blogUnitOfWork.CommentRepository.Update(postId, id, commentEntity);

            if (!isSucceed) return ("Like is not added successfully", false);

            return ("Like is added successfully", true);
        }

        public (string, bool) UnlikeComment(string postId, string id, string userId)
        {
            if (userId == null) return ("Failed to unlike: User Id is missing", false);

            User userEntity = _blogUnitOfWork.UserRepository.Get(userId);

            if (userEntity == null)
            {
                return ("Failed to unlike: User Id is not valid", false);
            }

            Comment commentEntity = _blogUnitOfWork.CommentRepository.Get(postId, id);

            if (commentEntity == null) return ("No Comment is found to unlike", false);

            if (!commentEntity.LikedUsers.Contains(userId)) return ("Failed to perform operation as no like is found with given User Id", false);

            commentEntity.LikedUsers.Remove(userId); // adding in array

            var isSucceed = _blogUnitOfWork.CommentRepository.Update(postId, id, commentEntity);

            if (!isSucceed) return ("Like is not removed successfully", false);

            return ("Like is removed successfully", true);
        }

        public (string, bool) UpdateCommentById(string postId, string id, CommentDto commentDto)
        {
            Comment entity = _blogUnitOfWork.CommentRepository.Get(postId, id);

            if (entity == null) return ("No Post is found to update", false);

            entity.Content = commentDto.Content;
            entity.Likes = commentDto.Likes; // need modification
            entity.UpdatedAt = DateTime.Now;

            var isSucceed = _blogUnitOfWork.CommentRepository.Update(postId, id, entity);

            if (!isSucceed) return ("Post is not updated successfully", false);

            return ("Post is updated successfully", true);
        }
    }
}